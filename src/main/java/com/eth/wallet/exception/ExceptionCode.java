package com.eth.wallet.exception;

import lombok.Getter;

/**
 * 异常码
 * Created by Niki on 2018/12/16 12:28
 */
@Getter
public enum ExceptionCode {
    ACCOUNT_CREATE_ERROR(1001,"生成账号异常"),
    ACCOUNT_KEYSTORE_ERROR(1002, "生成keystore文件异常"),
    ;
    private Integer code;
    private String msg;

    private ExceptionCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
