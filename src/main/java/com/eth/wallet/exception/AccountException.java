package com.eth.wallet.exception;

/**
 * 账号相关异常
 * Created by Niki on 2018/12/16 12:27
 */
public class AccountException extends RuntimeException {
    public AccountException(String message) {
        super(message);
    }
}
