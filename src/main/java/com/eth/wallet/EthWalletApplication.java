package com.eth.wallet;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EthWalletApplication {

    public static void main(String[] args) {
        SpringApplication.run(EthWalletApplication.class, args);
    }

}

