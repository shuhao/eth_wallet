package com.eth.wallet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Niki on 2018/12/13 19:41
 */
@Controller
public class IndexController {

    @GetMapping("/index")
    public String index(HttpServletRequest request) {
        request.setAttribute("key","good");
        return "/index";
    }
}
