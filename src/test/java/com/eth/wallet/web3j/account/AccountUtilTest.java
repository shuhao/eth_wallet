package com.eth.wallet.web3j.account;

import org.junit.Test;
import org.web3j.crypto.ECKeyPair;
import org.web3j.crypto.Keys;
import org.web3j.utils.Numeric;

import java.math.BigInteger;
import java.security.PublicKey;

import static org.junit.Assert.*;

/**
 * Created by Niki on 2018/12/16 12:39
 */
public class AccountUtilTest {



    @Test
    public void testPriKeyToKeystore() {
        String privateKey = "51000f41fcc64e7940d9fbf9de0f7fc23702781e10d28bf7cc7ac3f583155c67";
        ECKeyPair ecKeyPair = Keys.deserialize(privateKey.getBytes());

        String pk = ecKeyPair.getPrivateKey().toString(16);
        System.out.println("private key old：" + privateKey);
        System.out.println("private key new：" + privateKey);
    }

    @Test
    public void testPrivateKeyToPubKey() {
        String privateKey = "51000f41fcc64e7940d9fbf9de0f7fc23702781e10d28bf7cc7ac3f583155c67";
        BigInteger privateKeyInt = Numeric.toBigInt(privateKey);
        ECKeyPair ecKeyPair = ECKeyPair.create(privateKeyInt);
        BigInteger pubKeyInt = ecKeyPair.getPublicKey();
        String address = Keys.getAddress(pubKeyInt);
        System.out.println("private key is：" + privateKey);
        System.out.println("public key is：" + pubKeyInt);
        System.out.println("address key is：" + address);
    }

}